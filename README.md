# forkstat

logs process fork(), exec() and exit() activity

Read more here: [forkstat](https://github.com/ColinIanKing/forkstat)

Download forkstat package for Archlinux from here:
[faultstat](https://gitlab.com/archlinux_build/forkstat/-/jobs/artifacts/main/browse?job=run-build)

## Install

VERSION=0.03.02

```bash
sudo pacman -U forkstat-${VERSION}-1-x86_64.pkg.tar.zst

```
